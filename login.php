<?php
    require "conexao.php";
    $conn = conexao();
    session_start();


    if($_SERVER["REQUEST_METHOD"] == "POST"){
        $usuario = mysqli_real_escape_string($conn, $_POST['usuario']);
        $senha = mysqli_real_escape_string($conn, $_POST['senha']);

        $sql = "SELECT id from usuarios where usuario = '$usuario' and senha = sha1('$senha')";
        $result = mysqli_query($conn, $sql);
        $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
        $count = mysqli_num_rows($result);

        if($count == 1){
            session_start("meuusuario");
            $_SESSION['login_user'] = $usuario;
            header("location: index.php");
        } else {
            $error = "Nome de usuário ou senha inválido!";
        }
    }
?>
<html lang="pt-br">
    <head>
        <title>Login</title>
        <meta charset="UTF-8">
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="css/style.css">
        <style type="text/css">
            body {
                font-family: Arial, Helvetica, sans-serif;
                font-size: 14px;
            }
            label{
                display: inline-block;
                clear: left;
                width: 70px;
                text-align: right;
            }
            input {
                display: inline-block;
            }
            .box {
                border: #666666 solid 1px;
            }
        </style>
    </head>
    <body bgcolor="#FFFFFF">
        <div align="center" style="padding-top: 50px">
            <div style="width: 350px; border: solid 1px #333333;" align="left">
                <div style="background-color: #333333; color: #FFFFFF; padding: 3px;"><b><center>Login</center></b></div>
                <div style="margin: 30px">
                    <form action="" method="post">
                        <label>Usuário: </label> <input type="text" name="usuario" class="box"/> <br/><br/>
                        <label>Senha: </label> <input type="password" name="senha" class="box"/> <br/><br/>
                        <input type="submit" value="Enviar"/><br/>
                    </form>
                </div>
            </div>
        </div>
    </body>

</html>