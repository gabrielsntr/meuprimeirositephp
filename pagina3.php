<?php
    include('session.php');
?>
<!DOCTYPE html>
<html lang="pt-br"></html>
<html>
    <head>
        <title>Página 3</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="css/style.css">
        <style>
            body{
                background-color: #ADFF2F;
            }
        </style>
    </head>
    <body>
        <h3> Página 3 - Envio de Dados</h3>
        <form action="obrigado.php" method="POST">
            Nome: <br> <input type="text" name="nome" required><br>
            Email: <br> <input type="email" name="email" required><br>
            Celular: <br> <input type="text" name="celular" required><br>
            Observação: <br> <input type="textarea" name="observacao" required><br>
            <input type="submit" value="Enviar" required><br>
        </form>
    </body>
</html>
