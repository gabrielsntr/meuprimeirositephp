<?php 
    include('session.php');
?>
<!DOCTYPE html>
<html lang="pt-br"></html>
<html>
    <head>
        <title>Página 2</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="css/style.css">
        <style>
            body{
                background-color: #ADFF2F;
            }
        </style>
    </head>
    <body>
        <h1> Página 2 - Select *</h1>
        <p> Mussum Ipsum, cacilds vidis litro abertis. Quem manda na minha terra sou euzis! Vehicula non. Ut sed ex eros. Vivamus sit amet nibh non tellus tristique interdum. Viva Forevis aptent taciti sociosqu ad litora torquent. Em pé sem cair, deitado sem dormir, sentado sem cochilar e fazendo pose. </p>
        <?php
            $sql = "SELECT id, nome, email, celular from alunos";
            $result = $conn->query($sql);

            if($result->num_rows > 0){
                while($row = $result->fetch_assoc()){
                    echo "<br>=======================================================";
                    echo "<br>Id: ". $row["id"];
                    echo "<br>Nome: ". $row["nome"];
                    echo "<br>E-mail: ". $row["email"];
                    echo "<br>Celular: ". $row["celular"];
                    echo "<br>=======================================================";
                } 
            }else {
                echo "Não há registros.";
            }
            $conn->close();
            ?>
    </body>
</html>